from django.db import models

from datetime import datetime

# Create your models here.


class News(models.Model):
    title = models.CharField(max_length=100, blank=False)
    is_active = models.BooleanField()
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title