const timer = $(".header-time");

setInterval(() => {
    const time = new Date();
    timer.text(`${time.getHours()}:${time.getMinutes().toLocaleString(undefined, {minimumIntegerDigits: 2})}:${time.getSeconds().toLocaleString(undefined, {minimumIntegerDigits: 2})}`);
}, 1000)